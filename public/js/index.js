const form = $('#form');

form.on('submit', function (e) {
    e.preventDefault();
    clearMessages();
    sendForm(this);
})

function sendForm(form) {
    const data = new FormData(form);
    $.ajax({
        url: $(form).attr('action'),
        type: $(form).attr('method'),
        dataType: 'json',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.status) {
                addMessage('Successfully uploaded!');
                clearForm(form);
                return;
            }

            addErrors(data.errors);
        }
    })
}

function clearMessages() {
    $('#messages').empty();
}

function clearForm(form) {
    $(form).find("input[type=text], input[type=file]").val('');
}

function addMessage(message) {
    $('#messages').append('<p>' + message + '</p>');
}

function addErrors(errors) {
    $.each(errors, function (index, error) {
        addMessage('[' + index + '] ' + error.msg + '(code: ' + error.code + ')');
    });
}
