<?php

namespace App\Controller;

use App\Entity\Application;
use App\Service\FileUploader;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class IndexController extends AbstractController
{
    public function index(ValidatorInterface $validator): Response
    {
        return $this->render('index/index.html.twig');
    }

    public function store(Request $request, ValidatorInterface $validator, FileUploader $fileUploader)
    {
        $status = false;
        $aErrors = [];

        if ($request->isXmlHttpRequest() && $request->isMethod('POST')){
            $application = new Application(
                $request->get('name'),
                $request->get('surname'),
                $request->files->get('image')
            );

//            validate data
            $errors = $validator->validate($application);

            if (count($errors) === 0){
                try{
//                    check if file exist and upload
                    if ($sFilename = $request->files->get('image')){
                        $sFilename = $fileUploader->upload($sFilename);
                        $application->setImage($sFilename);
                    }

                    $em = $this->getDoctrine()->getManagerForClass(Application::class);
//                    rewrite image name
                    $application->setImage($sFilename);

                    $em->persist($application);
                    $em->flush();

                    $status = true;
                } catch (Exception $exception){
                    $aErrors['form'] = [
                        'msg' => 'Occured an error while uploading data.',
                        'code' => $exception->getCode()
                    ];
                }
            }

            foreach ($errors as $error){
                $aErrors[$error->getPropertyPath()] = [
                    'msg' => $error->getMessage(),
                    'code' => $error->getCode()
                ];
            }
        }

        return $this->json(['status' => $status, 'errors' => $aErrors]);
    }
}
