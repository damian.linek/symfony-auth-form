<?php

namespace App\Controller;

use App\Repository\ApplicationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class PanelController extends AbstractController
{
    public function index(ApplicationRepository $repository): Response
    {
        $applications = $repository->findAll();

        return $this->render('panel/index.html.twig', [
            'applications' => $applications,
        ]);
    }
}
